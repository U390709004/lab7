public class MyDate {
    private int day, month, year;

    MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public MyDate() {
        year = 1970;
        month = 1;
        day = 1;
    }

    public void incrementDay() {
        if (month == 2) {

            if ((year % 400 != 0 || (year % 4 != 0 && year % 100 == 0))
                    && day == 28) {
                day = 1;
                incrementMonth();
            } else if (day == 29) {
                day = 1;
                incrementMonth();
            } else
                day += 1;
        } else if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8
                || month == 10 || month == 12)) {
            if (day == 31) {
                day = 1;
                incrementMonth();
            } else
                day += 1;
        } else if ((month == 4 || month == 6 || month == 9 || month == 11)) {
            if (day == 30) {
                day = 1;
                incrementMonth();
            } else
                day += 1;
        }
    }

    private static boolean isLeapYear(int year) {
        return ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));
    }
    public void incrementMonth() {
        if (month == 12) {
            month = 1;
            incrementYear();
        } else
            month += 1;
    }
    int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    private int daysInMonth(int month) {
        if (month == 2 && isLeapYear(year))
            return monthDays[--month] + 1;
        return monthDays[--month];
    }

    public void incrementDay(int i) {
        for (; i > 0; i--)
            incrementDay();
    }


    public void incrementMonth(int i) {
        int years = i / 12;
        incrementYear(years);
        month += i - (12 * years);
        if (month > 12) {
            month = month - 12;
            incrementYear();
        }
        reset();
    }
    public void decrementMonth(int i) {
        int years = i / 12;
        decrementYear(years);
        month -= i - (12 * years);
        if (month < 1) {
            month = 12 + month;
            decrementYear();
        }
        reset();
    }


    public void incrementYear(int i) {
        year += i;
        reset();
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public void decrementDay(int i) {
        for (; i > 0; i--)
            decrementDay();
    }

    public void decrementDay() {
        if (day > 1)
            day--;
        else {
            decrementMonth();
            day = daysInMonth(month);
        }
    }



    public void decrementMonth() {
        decrementMonth(1);
    }

    public void decrementYear(int i) {
        year -= i;
        reset();
    }

    public void decrementYear() {

        decrementYear(1);
    }

    public boolean isAfter(MyDate anotherDate) {
        boolean result = true;
        if (year <= anotherDate.year) {
            if (year != anotherDate.year || month <= anotherDate.month) {
                result = year == anotherDate.year && month == anotherDate.month && day > anotherDate.day;
            }
        }
        return result;
    }

    public boolean isBefore(MyDate anotherDate) {
        if (year < anotherDate.year)
            return true;
        if (year == anotherDate.year && month < anotherDate.month)
            return true;
        return year == anotherDate.year && month == anotherDate.month && day < anotherDate.day;
    }

    public int dayDifference(MyDate anotherDate) {
        int days = 0;
        while (isAfter(anotherDate)) {
            decrementDay();
            days++;
        }
        while (isBefore(anotherDate)) {
            incrementDay();
            days++;
        }
        return days;
    }

    private void reset() {
        int days;
        days = daysInMonth(month);
        if (day > days)
            day = days;

    }
    @Override
    public String toString() {
        if (month < 10) return year + "-" + "0" + month + "-" + (day < 10 ? "0" : "") + day;
        return year + "-" + "" + month + "-" + (day < 10 ? "0" : "") + day;
    }
}